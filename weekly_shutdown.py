"""
Python support for Azure automation is now public preview!

Azure Automation documentation : https://aka.ms/azure-automation-python-documentation
Azure Python SDK documentation : https://aka.ms/azure-python-sdk

This tutorial runbook demonstrate how to authenticate against Azure using the Azure automation service principal and then lists the resource groups present in the specified subscription.
"""
import azure.mgmt.resource
import azure.mgmt.compute.compute
import automationassets

global resource_client
global compute_client


def get_automation_runas_credential(runas_connection):
    """ Returns credentials to authenticate against Azure resoruce manager """
    from OpenSSL import crypto
    from msrestazure import azure_active_directory
    import adal

    # Get the Azure Automation RunAs service principal certificate
    cert = automationassets.get_automation_certificate("AzureRunAsCertificate")
    pks12_cert = crypto.load_pkcs12(cert)
    pem_pkey = crypto.dump_privatekey(crypto.FILETYPE_PEM, pks12_cert.get_privatekey())

    # Get run as connection information for the Azure Automation service principal
    application_id = runas_connection["ApplicationId"]
    thumbprint = runas_connection["CertificateThumbprint"]
    tenant_id = runas_connection["TenantId"]

    # Authenticate with service principal certificate
    resource = "https://management.core.windows.net/"
    authority_url = ("https://login.microsoftonline.com/" + tenant_id)
    context = adal.AuthenticationContext(authority_url)
    return azure_active_directory.AdalAuthentication(
        lambda: context.acquire_token_with_client_certificate(
            resource,
            application_id,
            pem_pkey,
            thumbprint)
    )


def outputResult(longRunningOperation):
    print longRunningOperation


def weekly_shutdown_vm():
    # List Resource Groups
    print("--- Searching Resource Groups containing VM tagged with automation=WEEKLY_SHUTDOWN ---")

    # Loop through all the resource groups for given subscription
    for group in resource_client.resource_groups.list():
        print('\tResource Group {}'.format(group.name))
        process_resource_group(group)


def process_resource_group(group):
    # Get only virtual machine under resource group with 'automation' tag
    for vm in resource_client.resources.list_by_resource_group(
            group.name, filter="resourceType eq 'Microsoft.Compute/virtualMachines'"):
        process_virtual_machine(vm, group.name)


def process_virtual_machine(vm, resource_group_name):
    # Get the virtual machine image instance
    virtual_machine = compute_client.virtual_machines.get(resource_group_name, vm.name)

    # Get the 'automation' tags
    tags = virtual_machine.tags

    # If 'automation' exists as a VM tag
    if tags is not None and 'automation' in tags:

        auto_tags = [x.strip() for x in tags['automation'].encode('utf-8').split(',')]

        # stop(deallocate) VM
        if 'WEEKLY_SHUTDOWN' in auto_tags:
            print('\t  Found VM with tag, shutting down: ' + vm.name.encode('utf-8'))

            # stop(deallocate) the given vm. Returns an AzureOperationPoller
            # https://docs.microsoft.com/en-us/python/api/azure.mgmt.compute.compute.v2017_03_30.operations.virtualmachinesoperations?view=azure-python
            # compute_client.virtual_machines.deallocate(resource_group_name, vm.name)


# Authenticate to Azure using the Azure Automation RunAs service principal
runas_connection = automationassets.get_automation_connection("AzureRunAsConnection")
azure_credential = get_automation_runas_credential(runas_connection)

# Intialize the resource management client with the RunAs credential and subscription
resource_client = azure.mgmt.resource.ResourceManagementClient(
    azure_credential,
    str(runas_connection["SubscriptionId"]))

compute_client = azure.mgmt.compute.compute.ComputeManagementClient(
    azure_credential,
    str(runas_connection["SubscriptionId"]))

weekly_shutdown_vm()
